﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShotgunShell : Bullet
{
    [SerializeField] private Collider2D _damageCollider;

    private Vector3 _targetPosition;

    private void FixedUpdate()
    {
        if (transform.position != _targetPosition)
        {
            transform.position = Vector2.MoveTowards(transform.position, _targetPosition, Speed * Time.deltaTime);
        }
        else
        {
            _damageCollider.enabled = true;
            StartCoroutine(Hide());
        }
    }

    public override void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.TryGetComponent(out Enemy enemy))
        {
            enemy.TakeDamage(Damage);
        }
    }

    public override void SetTargetPosition(Vector2 target)
    {
        _targetPosition = target;
    }

    private IEnumerator Hide()
    {
        yield return new WaitForFixedUpdate();
        _damageCollider.enabled = false;
        Destroy(gameObject);
    }
}
