﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Animator), typeof(Enemy))]
public class ShootState : State
{
    [SerializeField] private Weapon _energyPower;
    [SerializeField] private Transform _shootPoint;

    private Animator _animator;
    private Enemy _enemy;

    private void Awake()
    {
        _enemy = GetComponent<Enemy>();
        _animator = GetComponent<Animator>();
    }

    private void OnEnable()
    {
        if (_enemy.Alive)
            _animator.Play("Attack");
    }

    public void Shoot()
    {
        _energyPower.Shoot(_shootPoint, Target.transform.position);
    }
}
