﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using TMPro;

public class NextWave : MonoBehaviour
{
    [SerializeField] private Spawner _spawner;
    [SerializeField] private Button _nextWaveButton;
    [SerializeField] private Image _nextWaveImage;
    [SerializeField] private float _nextWaveTime;
    [SerializeField] private int _waveRewardRate;
    [SerializeField] private CanvasGroup _rewardPanel;
    [SerializeField] private TMP_Text _rewardCount;
    [SerializeField] private float _rewardPanelHideTime;

    private float _timer;
    private IEnumerator _coroutine;

    public int RewardForWave { get; private set; }
    public event UnityAction<int> NextWaveSelected;

    private void OnEnable()
    {
        _spawner.AllEnemySpawned += OnAllEnemySpawned;
        _nextWaveButton.onClick.AddListener(OnNextWaveButtonClick);
    }

    private void OnDisable()
    {
        _spawner.AllEnemySpawned -= OnAllEnemySpawned;
        _nextWaveButton.onClick.RemoveListener(OnNextWaveButtonClick);
    }

    public void OnAllEnemySpawned()
    {
        _nextWaveButton.gameObject.SetActive(true);
        TimeNextWave();
    }

    public void OnNextWaveButtonClick()
    {
        _spawner.NextWave();
        StopRunningCoroutine();
        RewardForSelectWave();
        _nextWaveButton.gameObject.SetActive(false);

        if(RewardForWave > 0)
            ShowRewardPanel();
    }

    private void ShowRewardPanel()
    {
        _rewardPanel.alpha = 1;
        _rewardCount.text = "+" + RewardForWave;
        StartCoroutine(EndShowReward());
    }

    private void TimeNextWave()
    {
        StopRunningCoroutine();

        _coroutine = NextWaveTimer();
        StartCoroutine(_coroutine);
    }

    private void StopRunningCoroutine()
    {
        if (_timer > 0)
            StopCoroutine(_coroutine);
    }

    private void RewardForSelectWave()
    {
        RewardForWave = (int)_timer * _waveRewardRate;
        NextWaveSelected?.Invoke(RewardForWave);
    }

    private IEnumerator NextWaveTimer()
    {
        _timer = _nextWaveTime;

        while (_timer > 0)
        {
            yield return null;
            _timer -= Time.deltaTime;
            _nextWaveImage.fillAmount = _timer / _nextWaveTime;
        }

        _spawner.NextWave();
        _nextWaveButton.gameObject.SetActive(false);
        yield break;
    }

    private IEnumerator EndShowReward()
    {
        float delta = _rewardPanelHideTime;
        yield return new WaitForSeconds(0.5f);

        while (delta > 0)
        {
            yield return null;
            delta -= Time.deltaTime;
            _rewardPanel.alpha = delta / _rewardPanelHideTime;
        }

        _rewardCount.text = "";
        yield break;
    }
}
