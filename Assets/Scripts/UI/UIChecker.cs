﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class UIChecker : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    public bool ClickIsNotOnUI { get; private set; }

    private void Start()
    {
        ClickIsNotOnUI = true;
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        ClickIsNotOnUI = false;
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        ClickIsNotOnUI = true;
    }
}
