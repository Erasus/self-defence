﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class AK47 : Weapon
{
    [SerializeField] private float _offset;
    [Range(0, 2)]
    [SerializeField] private float _shootingSpread;

    public override void Shoot(Transform shootPoint, Vector3 shootTarget)
    {
        Vector3 difference = shootTarget - shootPoint.transform.position;
        float rotationZ = Mathf.Atan2(difference.y + Random.Range(-_shootingSpread, _shootingSpread), difference.x + Random.Range(-_shootingSpread, _shootingSpread)) * Mathf.Rad2Deg;
        shootPoint.transform.rotation = Quaternion.Euler(0f, 0f, rotationZ + _offset);

        Instantiate(Bullet, shootPoint.position, shootPoint.transform.rotation);
    }
}
