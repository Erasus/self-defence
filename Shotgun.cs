﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Shotgun : Weapon
{
    [SerializeField] private float _offset;

    public override void Hit()
    {
        
    }

    public override void Hit(List<Collider2D> collider2Ds)
    {
        throw new System.NotImplementedException();
    }

    //public override event UnityAction NeedReload;

    /*public override void Reload()
    {
        CurrentAmmo += MaxAmmoInMagasin;
    }*/

    public override void Shoot(Transform shootPoint, Vector3 shootTarget)
    {
        /*if (CurrentAmmo > 0)
        {*/
            Vector3 difference = shootTarget - shootPoint.transform.position;
            float rotationZ = Mathf.Atan2(difference.y, difference.x) * Mathf.Rad2Deg;
            shootPoint.transform.rotation = Quaternion.Euler(0f, 0f, rotationZ + _offset);

            Bullet bullet = Instantiate(Bullet, shootPoint.position, shootPoint.transform.rotation);
            SetBulletTarget(bullet, shootTarget);

            /*Instantiate(Bullet, shootPoint.position, shootPoint.transform.rotation);
            CurrentAmmo--;

            if (CurrentAmmo == 0)
            {
                NeedReload?.Invoke();
            }
        }*/
    }

    private void SetBulletTarget(Bullet bullet, Vector2 target)
    {
        //bullet.transform.position = transform.position;
        //bullet.transform.rotation = transform.rotation;
        bullet.SetTargetPosition(target);
    }
}
