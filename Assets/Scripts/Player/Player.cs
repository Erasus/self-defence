﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[RequireComponent(typeof(Animator))]
public class Player : MonoBehaviour
{
    [SerializeField] private int _health;
    [SerializeField] private List<Weapon> _weapons;
    [SerializeField] private Transform _shootPoint;
    [SerializeField] private UIChecker _uiChecker;
    [SerializeField] private MeleeCollisions _meleeCollisions;

    private Weapon _currentWeapon;
    private Vector3 _shootTarget;
    private int _currentWeaponNumber = 0;
    private int _currentHealth;
    private Animator _animator;
    private bool _canUseWeapon = true;
    
    public int Money { get; private set; }

    public event UnityAction<int, int> HealthChanged;
    public event UnityAction<int> MoneyChanged;
    public event UnityAction<Player> Dying;

    private void Start()
    {
        _animator = GetComponent<Animator>();
        ChangeWeapon(_weapons[_currentWeaponNumber]);
        _currentHealth = _health;
    }

    private void Update()
    {
        if(Input.GetMouseButton(0) && _uiChecker.ClickIsNotOnUI && _canUseWeapon)
        {
            if(_currentWeapon.WeaponType == WeaponType.Range)
            {
                _shootTarget = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                _animator.Play(_currentWeapon.Name + "Shoot");
            }
            else if(_currentWeapon.WeaponType == WeaponType.Melee)
            {
                _animator.Play(_currentWeapon.Name + "Hit");
            }
        }
    }

    public void Shoot()
    {
        _currentWeapon.Shoot(_shootPoint, _shootTarget);
    }

    public void ApplyDamage(int damage)
    {
        _currentHealth -= damage;
        HealthChanged?.Invoke(_currentHealth, _health);

        if (_currentHealth <= 0)
        {
            Dying?.Invoke(this);
            Destroy(gameObject);
        }
    }

    public void AddMoney(int reward)
    {
        Money += reward;
        MoneyChanged?.Invoke(Money);
    }

    public void BuyWeapon(Weapon weapon)
    {
        Money -= weapon.Price;
        _weapons.Add(weapon);
        MoneyChanged?.Invoke(Money);
    }

    public void NextWeapon()
    {
        if(_canUseWeapon)
        {
            if (_currentWeaponNumber == _weapons.Count - 1)
                _currentWeaponNumber = 0;
            else
                _currentWeaponNumber++;

            _animator.SetBool(_currentWeapon.Name, false);
            ChangeWeapon(_weapons[_currentWeaponNumber]);
        }
    }

    public void PreviousWeapon()
    {
        if (_canUseWeapon)
        {
            if (_currentWeaponNumber == 0)
                _currentWeaponNumber = _weapons.Count - 1;
            else
                _currentWeaponNumber--;

            _animator.SetBool(_currentWeapon.Name, false);
            ChangeWeapon(_weapons[_currentWeaponNumber]);
        }
    }

    private void ChangeWeapon(Weapon weapon)
    {
        _currentWeapon = weapon;
        _animator.SetBool(_currentWeapon.Name, true);

        if (weapon.WeaponType == WeaponType.Melee)
        {
            _meleeCollisions.SetWeapon(_currentWeapon.GetComponent<Axe>());
        }
    }
}
