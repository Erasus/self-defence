﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Shotgun : Weapon
{
    [SerializeField] private float _offset;

    public override void Shoot(Transform shootPoint, Vector3 shootTarget)
    {
        Vector3 difference = shootTarget - shootPoint.transform.position;
        float rotationZ = Mathf.Atan2(difference.y, difference.x) * Mathf.Rad2Deg;
        shootPoint.transform.rotation = Quaternion.Euler(0f, 0f, rotationZ + _offset);

        Bullet bullet = Instantiate(Bullet, shootPoint.position, shootPoint.transform.rotation);
        SetBulletTarget(bullet, shootTarget);
    }

    private void SetBulletTarget(Bullet bullet, Vector2 target)
    {
        bullet.SetTargetPosition(target);
    }
}
