﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Spawner : MonoBehaviour
{
    [SerializeField] private float _horizontalSpawnSpread;
    [SerializeField] private float _verticalSpawnSpread;
    [SerializeField] private List<Wave> _waves;
    [SerializeField] private Transform _spawnPoint;
    [SerializeField] private Transform[] _flyingPoints;
    [SerializeField] private Player _player;
    [SerializeField] private NextWave _nextWave;

    private Wave _currentWave;
    private int _currentWaveNumber = 0;
    private float _timeAfterLastSpaw;
    private int _spawned;
    private bool _gameOver = false;

    public event UnityAction AllEnemySpawned;
    public event UnityAction<int, int> EnemyCountChanged;

    private void Start()
    {
        SetWave(_currentWaveNumber);
        _player.Dying += OnPlayerDying;
    }

    private void Update()
    {
        if (_currentWave == null || _gameOver)
            return;

        _timeAfterLastSpaw += Time.deltaTime;

        if(_timeAfterLastSpaw >= _currentWave.Delay)
        {
            InstantiateEnemy();
            _spawned++;
            _timeAfterLastSpaw = 0;
        }

        if(_currentWave.Template.Length <= _spawned)
        {
            if (_waves.Count > _currentWaveNumber + 1)
                AllEnemySpawned?.Invoke();

            _currentWave = null;
        }
    }

    private void InstantiateEnemy()
    {
        float spreadX = Random.Range(-_horizontalSpawnSpread, _horizontalSpawnSpread);
        float spreadY = Random.Range(-_verticalSpawnSpread, _verticalSpawnSpread);

        Enemy enemy = Instantiate(_currentWave.Template[_spawned], _spawnPoint.position, _spawnPoint.rotation, _spawnPoint).GetComponent<Enemy>();
        enemy.transform.position = new Vector3(_spawnPoint.transform.position.x + spreadX, _spawnPoint.transform.position.y + spreadY, _spawnPoint.transform.position.y + spreadY);
        enemy.Init(_player);

        if (enemy.EnemyType == EnemyType.Flying) 
            enemy.GetFlyingPoints(_flyingPoints);

        enemy.Dying += OnEnemyDying;

        
    }

    private void SetWave(int index)
    {
        _nextWave.NextWaveSelected += OnNextWaveSelected;
        _currentWave = _waves[index];

        EnemyCountChanged?.Invoke(index + 1, _waves.Count);
    }

    public void NextWave()
    {
        _nextWave.NextWaveSelected -= OnNextWaveSelected;
        SetWave(++_currentWaveNumber);
        _spawned = 0;
    }

    private void OnEnemyDying(Enemy enemy)
    {
        enemy.Dying -= OnEnemyDying;
        _player.AddMoney(enemy.Reward);
    }
    
    private void OnNextWaveSelected(int reward)
    {
        _player.AddMoney(reward);
    }

    private void OnPlayerDying(Player player)
    {
        _gameOver = true;
        player.Dying -= OnPlayerDying;
    }
}

[System.Serializable]
public class Wave
{
    public GameObject[] Template;
    public float Delay;
}
