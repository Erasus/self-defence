﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Axe : Weapon
{
    [SerializeField] private int _damage;

    public int Damage => _damage;

    public override void Shoot(Transform shootPoint, Vector3 shootTarget)
    {
        
    }
}
