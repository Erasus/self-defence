﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnergyBullet : Bullet
{
    private void Update()
    {
        transform.Translate(Vector2.left * Speed * Time.deltaTime);
    }

    public override void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.TryGetComponent(out Player player))
        {
            player.ApplyDamage(Damage);

            Destroy(gameObject);
        }
    }

    public override void SetTargetPosition(Vector2 target)
    {
        
    }
}
