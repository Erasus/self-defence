﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackToFlyPathTransition : Transition
{
    public void TransitToFlyPath()
    {
        NeedTransit = true;
    }
}
