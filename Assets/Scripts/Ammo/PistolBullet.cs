﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PistolBullet : Bullet
{
    private void Update()
    {
        transform.Translate(Vector2.left * Speed * Time.deltaTime);
    }

    public override void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.TryGetComponent(out Enemy enemy))
        {
            enemy.TakeDamage(Damage);

            Destroy(gameObject);
        }
    }

    public override void SetTargetPosition(Vector2 target)
    {
        throw new System.NotImplementedException();
    }
}
