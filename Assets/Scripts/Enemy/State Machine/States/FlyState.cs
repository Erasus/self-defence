﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Enemy))]
public class FlyState : State
{
    [SerializeField] private float _speed;

    private Transform[] _flyingPoints;
    private Transform _nextPoint;
    private Enemy _enemy;

    private void Start()
    {
        _enemy = GetComponent<Enemy>();
        _flyingPoints = _enemy.SetFlyingPoints();
        _nextPoint = _flyingPoints[Random.Range(0, _flyingPoints.Length)];
    }


    private void Update()
    {
        if (_enemy.Alive)
        {
            if(transform.position != _nextPoint.position)
            {
                transform.position = Vector3.MoveTowards(transform.position, _nextPoint.position, _speed * Time.deltaTime);
            }
            else
            {
                _nextPoint = _flyingPoints[Random.Range(0, _flyingPoints.Length)];
            }
        }
    }
}
