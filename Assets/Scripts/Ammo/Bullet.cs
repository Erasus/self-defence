﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Bullet : MonoBehaviour
{
    [SerializeField] protected int Damage;
    [SerializeField] protected float Speed;

    private void Start()
    {
        Destroy(gameObject, 2.5f);
    }

    public abstract void OnTriggerEnter2D(Collider2D collision);
    public abstract void SetTargetPosition(Vector2 target);
}
