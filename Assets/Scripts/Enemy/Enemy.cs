﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[RequireComponent(typeof(Animator))]
public class Enemy : MonoBehaviour
{
    [SerializeField] private EnemyType _enemyType;
    [SerializeField] private int _health;
    [SerializeField] private int _reward;

    private Player _target;
    private Transform[] _flyingPoints;
    private Animator _animator;

    public EnemyType EnemyType => _enemyType;
    public int Reward => _reward;
    public Player Target => _target;
    public bool Alive { get; private set; } = true;

    public event UnityAction<Enemy> Dying;

    private void Start()
    {
        _animator = GetComponent<Animator>();
    }

    public void Init(Player target)
    {
        _target = target;
    }

    public void GetFlyingPoints(Transform[] points)
    {
        _flyingPoints = points;
    }

    public Transform[] SetFlyingPoints()
    {
        return _flyingPoints;
    }

    public void TakeDamage(int damage)
    {
        _health -= damage;

        if (_health <= 0)
        {
            Dying?.Invoke(this);
            Alive = false;
            _animator.Play("Die");
        }
        else
        {
            _animator.Play("Damaged");
        }
    }

    public void Die()
    {
        Destroy(gameObject);
    }
}

[System.Serializable]
public enum EnemyType
{
    Walking,
    Flying
}
