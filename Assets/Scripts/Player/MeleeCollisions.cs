﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeleeCollisions : MonoBehaviour
{
    private Axe _weapon;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.TryGetComponent(out Enemy enemy))
        {
            enemy.TakeDamage(_weapon.Damage);
        }
    }

    public void SetWeapon(Axe weapon)
    {
        _weapon = weapon;
    }
}
