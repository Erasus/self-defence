﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public abstract class Bar : MonoBehaviour
{
    [SerializeField] private float _speed;

    [SerializeField] protected Slider Slider;

    private bool _coroutineIsBegin = false;
    private IEnumerator _coroutine;

    public void OnValueChanged(int value, int maxValue)
    {
        if (_coroutineIsBegin)
        {
            StopCoroutine(_coroutine);
            _coroutineIsBegin = false;
        }

        _coroutine = ChangeSliderValue(value, maxValue);
        StartCoroutine(_coroutine);
    }

    private IEnumerator ChangeSliderValue(float value, float maxValue)
    {
        _coroutineIsBegin = true;
        float sliderChangedValue = value / maxValue;

        while (Slider.value != sliderChangedValue)
        {
            Slider.value = Mathf.MoveTowards(Slider.value, sliderChangedValue, _speed * Time.deltaTime);
            yield return null;
        }

        _coroutineIsBegin = false;
    }
}
