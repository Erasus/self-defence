﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public abstract class Weapon : MonoBehaviour
{
    [SerializeField] private string _name;
    [SerializeField] private string _label;
    [SerializeField] private WeaponType _weaponType;
    [SerializeField] private int _price;
    [SerializeField] private Sprite _icon;
    [SerializeField] private bool _isBuyed = false;

    [SerializeField] protected Bullet Bullet;

    public string Name => _name;
    public string Label => _label;
    public WeaponType WeaponType => _weaponType;
    public int Price => _price;
    public Sprite Icon => _icon;
    public bool IsBuyed => _isBuyed;

    public abstract void Shoot(Transform shootPoint, Vector3 shootTarget);

    public void Buy()
    {
        _isBuyed = true;
    }
}

public enum WeaponType
{
    Melee,
    Range
}
